Shield-LEDx3 - Tinusaur Shield-LEDx3 Project

-----------------------------------------------------------------------------------
 Copyright (c) 2017 Neven Boyanov, The Tinusaur Team. All Rights Reserved.
 Distributed as open source software under MIT License, see LICENSE.txt file.
 Retain in your source code the link http://tinusaur.org to the Tinusaur project.
-----------------------------------------------------------------------------------

This is code for the Tinusaur Shield-LEDx3 project.

Folders and modules:
- shield_ledx3         - Library
- shield_ledx3_blink1  - 1 blinking LED
- shield_ledx3_blink3  - 3 blinking LEDs
- shield_ledx3_fade1   - 1 fading in/out LED
- shield_ledx3_fade3   - 3 fading in/out LEDs
- shiled_ledx3_util    - images and photos

This was developed for and tested on the ATtiny85 microcontroller.

==== Links ====

Official Tinusaur Project website: http://tinusaur.org
Project Shield-LEDx3 page: https://tinusaur.org/products/shields/tinusaur-shield-ledx3/
Project Shield-LEDx3 source code: https://bitbucket.org/tinusaur/shield-ledx3/

Twitter: https://twitter.com/tinusaur
Facebook: https://www.facebook.com/tinusaur

