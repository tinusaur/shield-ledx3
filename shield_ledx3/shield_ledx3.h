/**
 * Shield-LEDx3
 *
 * @created 2017-03-14
 * @author Neven Boyanov
 *
 * This is part of the Tinusaur/Shield-LEDx3 project.
 *
 * Copyright (c) 2017 Neven Boyanov, Tinusaur Team. All Rights Reserved.
 * Distributed as open source software under MIT License, see LICENSE.txt file.
 * Retain in your source code the link http://tinusaur.org to the Tinusaur project.
 *
 * Source code available at: https://bitbucket.org/tinusaur/shield-ledx3
 *
 */

// ----------------------------------------------------------------------------

#ifndef SHIELDLEDX3_H
#define SHIELDLEDX3_H

// ----------------------------------------------------------------------------

#include <stdint.h>

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~ Shield-LEDx3 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//               Shield LEDx3
//                  +-------+
//      (RST)-->    +   Vcc +---(+)--VCC--
// --[OWOWOD]--> +--+   PB2 +---LED2------ (GREEN)
// --------------+ PB4  PB1 +---LED2------ (YELLOW)
// --------(-)---+ GND  PB0 +---LED1------ (RED)
//               +----------+
//                 Tinusaur
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Ports definitions

#define SHIELD_LEDX3_LED1 PB0	// Define the LED1 (RED) I/O port
#define SHIELD_LEDX3_LED2 PB1	// Define the LED2 (YELLOW) I/O port
#define SHIELD_LEDX3_LED3 PB2	// Define the LED3 (GREEN) I/O port

// ----------------------------------------------------------------------------

#endif

// ----------------------------------------------------------------------------
