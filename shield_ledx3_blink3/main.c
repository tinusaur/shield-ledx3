/**
 * Shield-LEDx3 - Test
 *
 * @created 2017-03-07
 * @author Neven Boyanov
 *
 * This is part of the Tinusaur/Shield-LEDx3 project.
 *
 * Copyright (c) 2017 Neven Boyanov, Tinusaur Team. All Rights Reserved.
 * Distributed as open source software under MIT License, see LICENSE.txt file.
 * Retain in your source code the link http://tinusaur.org to the Tinusaur project.
 *
 * Source code available at: https://bitbucket.org/tinusaur/shield-ledx3
 *
 */

// ============================================================================

// #define F_CPU 1000000UL
// NOTE: The F_CPU (CPU frequency) should not be defined in the source code.
//       It should be defined in either (1) Makefile; or (2) in the IDE. 

#include <stdlib.h>
#include <stdint.h>

#include <avr/io.h>
#include <util/delay.h>

#include "shield_ledx3/shield_ledx3.h"

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~ Shield-LEDx3 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//               Shield LEDx3
//                  +-------+
//      (RST)-->    +   Vcc +---(+)--VCC--
// --[OWOWOD]--> +--+   PB2 +---LED2------ (GREEN)
// --------------+ PB4  PB1 +---LED2------ (YELLOW)
// --------(-)---+ GND  PB0 +---LED1------ (RED)
//               +----------+
//                 Tinusaur
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

int main(void) {

	// ---- Initialization ----
	
	// Set the LED port(s) as output.
	DDRB |= (1 << SHIELD_LEDX3_LED1);
	DDRB |= (1 << SHIELD_LEDX3_LED2);
	DDRB |= (1 << SHIELD_LEDX3_LED3);
	
	// ---- Testing: Blink 3 LEDs ----
	
	for (;;) { // Infinite main loop
		PORTB |= (1 << SHIELD_LEDX3_LED1);
		PORTB &= ~(1 << SHIELD_LEDX3_LED2);
		PORTB &= ~(1 << SHIELD_LEDX3_LED3);
		_delay_ms(800);

		PORTB &= ~(1 << SHIELD_LEDX3_LED1);
		PORTB |= (1 << SHIELD_LEDX3_LED2);
		PORTB &= ~(1 << SHIELD_LEDX3_LED3);
		_delay_ms(400);
		
		PORTB &= ~(1 << SHIELD_LEDX3_LED1);
		PORTB &= ~(1 << SHIELD_LEDX3_LED2);
		PORTB |= (1 << SHIELD_LEDX3_LED3);
		_delay_ms(200);
	}

	// Return the mandatory for the "main" function int value. It is "0" for success.
	return 0;
}

// ============================================================================
